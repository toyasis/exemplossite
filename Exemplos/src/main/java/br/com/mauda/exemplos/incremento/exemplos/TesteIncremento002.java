package br.com.mauda.exemplos.incremento.exemplos;

import org.junit.Test;

public class TesteIncremento002 { 
	
	@Test
	public void teste(){
		int variavel = 0;
		System.out.println("Valor inicial da variavel = " + variavel);
		System.out.println("Pr� incremento na variavel = " + ++variavel);
		System.out.println("P�s incremento na variavel = " + variavel++);
		System.out.println("Valor final da variavel = " + variavel);
	}
}